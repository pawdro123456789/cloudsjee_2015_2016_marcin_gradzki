#!/bin/bash

scp -r vagrant@hadoop-master:/usr/local/hadoop /home/vagrant/hadoop
rm -rf /usr/local/hadoop
mv /home/vagrant/hadoop /usr/local/
sudo chown vagrant:hadoop -R /usr/local/hadoop