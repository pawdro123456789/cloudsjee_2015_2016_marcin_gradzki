#!/bin/bash

sudo apt-get update
sudo apt-get install unzip -y
sudo mv /home/vagrant/archives.zip /var/cache/apt/archives/
cd /var/cache/apt/archives/
sudo unzip archives.zip
sudo mv archives/*.deb .
sudo rm -rf archives