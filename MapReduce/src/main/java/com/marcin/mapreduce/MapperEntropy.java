package com.marcin.mapreduce;

import java.io.IOException;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;



public class MapperEntropy extends Mapper<Object, Text, DoubleWritable, Text> {

    private final static IntWritable zero = new IntWritable(0);
    private Text word = new Text();
   
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
        double val = val(value);
        context.write(new DoubleWritable(val), value);
    }
    
    private static double val(Text key){
        String line = key.toString();
        String abc = "[a-z]";
        String ABC = "[A-Z]";
        String num = "[0-9]";
        String sym = "[^a-zA-Z0-9]";
        double n = 0.0;

        Pattern p = Pattern.compile(abc);
        Pattern q = Pattern.compile(ABC);
        Pattern r = Pattern.compile(num);
        Pattern s = Pattern.compile(sym);

        Matcher w = p.matcher(line);
        Matcher x = q.matcher(line);
        Matcher y = r.matcher(line);
        Matcher z = s.matcher(line);

        if (w.find()) n += 26.0;
        if (x.find()) n += 26.0;
        if (y.find()) n += 10.0;
        if (z.find()) n += 32.0;

        return n = line.length() * (Math.log(n) / Math.log(2));
    }
   
}
